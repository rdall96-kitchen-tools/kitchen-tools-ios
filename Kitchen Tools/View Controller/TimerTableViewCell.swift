//
//  TimerTableViewCell.swift
//  Kitchen Tools
//
//  Created by Ricky Dall'Armellina on 3/16/19.
//  Copyright © 2019 Rdall96. All rights reserved.
//

import UIKit

class TimerTableViewCell: UITableViewCell {
    
    
    // MARK: UI Elements
    
    @IBOutlet weak var timerNameLabel: UILabel!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var timerPauseBtn: UIButton!
    
    
    // MARK: Data
    var timer: TimeKeeper!
    private var timerViewUpdater: Timer? {
        willSet {
            self.timerViewUpdater?.invalidate()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Setup a timer that updates the timer view every half second
        self.timerViewUpdater = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: UI Actions
    
    @IBAction func timerPauseBtnClicked(_ sender: UIButton) {
        switch timer.isRunning {
        case true:
            timer.pause()
            pauseUpdater()
            timerPauseBtn.setTitle("Resume", for: .normal)
        default:
            switch timer.isDone {
            case false:
                timer.start()
                runUpdater()
            default:
                timer.reset()
                runUpdater()
                timer.start()
            }
            timerPauseBtn.setTitle("Pause", for: .normal)
        }
    }
    
    private func runUpdater() {
        updateTimer()
        self.timerViewUpdater = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    func pauseUpdater() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            self.timerViewUpdater = nil
        })
    }
    
    @objc func updateTimer() {
        // Display the current timer value
        timeLeftLabel.text = timer.formattedTime
        // Change label colors and state depending if timer is running
        switch timer.isRunning {
        case true:
            timeLeftLabel.textColor = .darkText
        default:
            switch timer.isDone {
            case true:
                timeLeftLabel.textColor = .green
                timer.stop()
                pauseUpdater()
                timerPauseBtn.setTitle("Reset", for: .normal)
                // TODO: Display done alert!
            default:
                timeLeftLabel.textColor = UIColor.gray
            }
        }
    }
    

}
