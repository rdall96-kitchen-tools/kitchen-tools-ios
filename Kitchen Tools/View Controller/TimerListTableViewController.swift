//
//  TimerListTableViewController.swift
//  Kitchen Tools
//
//  Created by Ricky Dall'Armellina on 3/16/19.
//  Copyright © 2019 Rdall96. All rights reserved.
//

import UIKit

class TimerListTableViewController: UITableViewController {
    
    
    // MARK: UI Elements
    
    @IBOutlet var timerTable: UITableView!
    
    
    // MARK: Data
    
    var timers: [TimeKeeper] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        tableView.dataSource = self
        tableView.delegate = self
        
        // Create fake timer as example
        timers.append(TimeKeeper(name: "Example #1", time: 60, start: false))
        timers.append(TimeKeeper(name: "Example #2", time: 120, start: false))
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return timers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "timerCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TimerTableViewCell
            else {
                fatalError("The dequeued cell is not an instanceof \(cellIdentifier)")
        }

        // Fetch proper timer to display in cell
        cell.timerNameLabel.text = timers[indexPath.row].name
        cell.timeLeftLabel.text = timers[indexPath.row].formattedTime
        cell.timer = timers[indexPath.row]
        
        // Check if timer was set to auto-start at creation and add proper button label
        if(timers[indexPath.row].isRunning) {
            cell.timerPauseBtn.setTitle("Pause", for: .normal)
        }

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Stop and invalidate the timer for that row
            (tableView.cellForRow(at: indexPath) as! TimerTableViewCell).pauseUpdater()
            timers[indexPath.row].stop()
            // Delete the row from the data source
            timers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier ?? "" {
        
        case "EditTimer":
            // Edit existing timer mode
            
            guard let timerDetailViewController = segue.destination as? TimerViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            guard let selectedTimerCell = sender as? TimerTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            guard let indexPath = timerTable.indexPath(for: selectedTimerCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedTimer = timers[indexPath.row]
            selectedTimer.pause()
            timerDetailViewController.timer = selectedTimer
        
        case "AddTimer":
            // Add new timer mode
            guard let addTimerNavigation = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            guard let addTimerViewController = addTimerNavigation.children.first as? TimerViewController else {
                fatalError("Unexpected destination \(segue.destination)")
            }
            addTimerViewController.timerCnt = timers.count
            
        default:
            fatalError("Unexpected segue \(String(describing: segue.identifier))")
            
        }
        
    }
    
    
    // MARK: UI Actions
    @IBAction func unwindToTimerList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? TimerViewController, let timer = sourceViewController.timer {
            
            // Update existing timer
            if let selectedIndexPath = timerTable.indexPathForSelectedRow {
                timers[selectedIndexPath.row] = timer
                timerTable.reloadRows(at: [selectedIndexPath], with: .none)
            }
            else {
                // Add new timer
                let newIndexPath = IndexPath(row: timers.count, section: 0)
                timers.append(timer)
                timerTable.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
        
    }

}
