//
//  TimerViewController.swift
//  Kitchen Tools
//
//  Created by Ricky Dall'Armellina on 3/16/19.
//  Copyright © 2019 Rdall96. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    
    // MARK: UI Elements
    
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var timerNameTextField: UITextField!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var startTimerSwitch: UISwitch!
    
    
    // Timer data to edit, eother sent in by TimerListTableViewController in prepareForSegue or to create a new timer
    var timer: TimeKeeper?
    var timerCnt: Int?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup data if editing an existing timer
        if let timer = timer {
            timerNameTextField.text = timer.name
            startTimerSwitch.isEnabled = false
            timePicker.countDownDuration = TimeInterval(timer.getDefaultTime())
        }
        else {
            // If there is no timer to edit, then assign default values for the current amount of timers in the table
            let count = (timerCnt ?? 0) + 1
            timerNameTextField.placeholder = "Timer \(count)"
            timePicker.countDownDuration = 0
        }
    }
    
    // MARK: - Navigation
    
    // Segue triggerred to send data across controllers
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // This should onyl execute when the SAVE button is pressed
        guard let button = sender as? UIBarButtonItem, button === saveBtn else {
            return
        }
        
        let name: String = timerNameTextField.text ?? timerNameTextField.placeholder ?? "Timer"
        timer = TimeKeeper(
            name: name,
            time: Int(timePicker.countDownDuration),
            start: startTimerSwitch.isOn
        )
    }
    
    
    // MARK: UI Actions
    
    @IBAction func cancelBtnClicked(_ sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways
        let isPresentingInAddTimerMode = presentingViewController is UINavigationController
        if isPresentingInAddTimerMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The TimerViewController is not inside a navigation controller")
        }
    }
    
    @IBAction func saveBtnClicked(_ sender: UIBarButtonItem) {
        // Grab info and send it back the ladder to create a new timer
        
    }

}
