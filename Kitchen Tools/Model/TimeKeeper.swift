//
//  TimeKeeper.swift
//  Kitchen Tools
//
//  Created by Ricky Dall'Armellina on 3/16/19.
//  Copyright © 2019 Rdall96. All rights reserved.
//

import Foundation

class TimeKeeper {
    
    // Data
    
    var name: String
    private var time: Int // time left in seconds
    private var startTime: Int // initial start time in seconds
    private var timer: Timer? { // swift timer that updates the time every second
        willSet {
            timer?.invalidate()
        }
    }
    var isRunning: Bool
    
    
    // Initializer
    
    init(name: String?, time: Int, start: Bool?) {
        
        self.name = name ?? "Timer"
        self.time = time
        self.startTime = time
        
        // Start the background countdown if start = true
        switch start {
        case true:
            isRunning = true
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        default:
            isRunning = false
        }
        
    }
    
    
    // Methods
    
    func start() {
        if(!isRunning) {
            isRunning = true
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        }
    }
    
    func stop() {
        isRunning = false
        self.timer = nil
        self.time = 0
    }
    
    func pause() {
        isRunning = false
        timer = nil
    }
    
    func reset() {
        time = startTime
    }
    
    @objc func tick() {
        switch time {
        case ...0:
            stop()
        default:
            time -= 1
        }
    }
    
    func addTime(seconds: Int) {
        time += seconds
    }
    
    var isDone: Bool {
        switch time {
        case ...0:
            return true
        default:
            return false
        }
    }
    
    var formattedTime: String {
        
        let formattedTime: String!
        
        switch isDone {
        case false:
            let hours = time / 3600
            let minutes = time / 60 % 60
            let seconds = time % 60
            formattedTime = String(format: "%02i:%02i:%02i", hours, minutes, seconds)
        default:
            formattedTime = "DONE"
        }
        
        return formattedTime
    }
    
    func getDefaultTime() -> Int { return startTime }
    
    func getHours() -> Int { return startTime / 3600 }
    
    func getMinutes() -> Int { return startTime / 60 % 60 }
    
    func getSeconds() -> Int { return startTime % 60 }
    
    
}
