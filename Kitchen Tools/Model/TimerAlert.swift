//
//  TimerAlert.swift
//  Kitchen Tools
//
//  Created by Ricky Dall'Armellina on 3/19/19.
//  Copyright © 2019 Rdall96. All rights reserved.
//

import Foundation
import UIKit

class TimerAlert {
    
    static func createEndTimerAlert(forTimer timer: TimeKeeper) -> UIAlertController {
        
        // Create alert
        let alert = UIAlertController(
            title: "Time's up!",
            message: "\(timer.name) is done.",
            preferredStyle: .alert
        )
        
        // STOP action
        alert.addAction(UIAlertAction(
            title: "Stop",
            style: .cancel,
            handler: { action in
                timer.stop()
                // TODO: Stop alert sound
        }))
        
        // SNOOZE action
//        alert.addAction(UIAlertAction(
//            title: "Add 1 minute",
//            style: .default,
//            handler: { action in
//                timer.addTime(60)
//        }))
        
        // Return alert
        return alert
        
    }
    
}
